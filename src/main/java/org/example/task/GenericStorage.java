package org.example.task;

public class GenericStorage <T extends Person>{

    private Person[] storage;

    private int lastIndexOfFilledCapacity = 0;// last index of filled capacity of array. Indexes after this contain
    // null values in the array

    public GenericStorage() {
        storage = new Person[2];// create array which can hold two items
    }

    private void resizeArray(){
        if(storage.length - lastIndexOfFilledCapacity < 2)// if array is about to be full, increase the size
        {
            storage = copyArray(storage.length + 3);
        }
        else if(storage.length - lastIndexOfFilledCapacity > 4){ // if array contains unnecessary space, reduce size
            storage = copyArray(storage.length - 2);
        }
    }


    private Person[] copyArray(int length){// copy storage array to another array whose length can be passed
        this.lastIndexOfFilledCapacity = 0;
        Person[] temp = new Person[length];
        for(Person p : storage){
            if(p != null) {
                temp[lastIndexOfFilledCapacity++] = p;
            }
        }
        return temp;
    }

    private boolean checkIfTheSameObjectHasBeenAdded(T item){
        // check whether the same object already exists in the array. It guarantees uniqueness of id's.
        // If not provided, the same object could be stored in array multiple
        // times and there would be multiple duplicated id's
        for(Person p : storage){
            if(p != null && p.equals(item)){
                // in my case I only check whether the objects are same.
                // I didn't override equals(), because I can save two people with the same name and the same age
                // to the storage but with different id's
                // two teachers can have same name and age in the storage, as for two students (only id's are unique)
                return true;
            }
        }
        return false;
    }

    public void addItem(T item){
        if(item != null && !checkIfTheSameObjectHasBeenAdded(item)) {
            // it checks whether the object is null. If null, it doesn't store it
            // first method explained above
            resizeArray();// check array size before adding a new item to it, and increase its size if needed
            storage[lastIndexOfFilledCapacity++] = item;
        }
    }

    public void displayAllItems(){
        for(int i = 0; i < lastIndexOfFilledCapacity; i++){
            System.out.println(storage[i]);
        }
    }

    public T searchItem(int id){
        T searched = null;
        for(Person item : storage){// searched item is found with the given id and returned
            if(item != null && item.getId() == id){
                searched = (T) item;
                break;
            }
        }
        return searched;
    }

    public void removeItem(int id){
        for(int i = 0; i < storage.length; i++){// if item with the given id is found, it is assigned to null
            if(storage[i] != null && storage[i].getId() == id){
                storage[i] = null;
                break;
            }
        }
        // then array is copied to another temporary array by skipping null values
        storage = copyArray(storage.length);// after copying, it takes the reference back
        resizeArray();// array reduces its size to save space in memory (unnecessary space is eliminated)
    }
}
