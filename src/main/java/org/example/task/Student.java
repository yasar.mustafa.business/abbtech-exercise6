package org.example.task;

public class Student extends Person {
    public Student(String name, int age) {
        super(name, age);
    }

    @Override
    boolean isValidAge(int age) {
        return age > 18 && age < 35;
    }

    @Override
    public String toString() {
        return "Student-> " + super.toString();
    }


}
