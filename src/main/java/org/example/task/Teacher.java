package org.example.task;

public class Teacher extends Person{
    public Teacher(String name, int age) {
        super(name, age);
    }

    @Override
    boolean isValidAge(int age) {
        return age > 25 && age < 65;
    }

    @Override
    public String toString() {
        return "Teacher-> " + super.toString();
    }
}
