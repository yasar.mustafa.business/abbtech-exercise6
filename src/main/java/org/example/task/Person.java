package org.example.task;

import java.util.Objects;

public abstract class Person {
    private static int idIncrement = 0;
    private int id;
    private String name;
    private int age;

    public Person(String name, int age) {
        setName(name);
        setAge(age);
        this.id = idIncrement++;// unique id each time a new object is created
    }

    private boolean isValidName(String name){// Set validation rule for name
        return name.matches("^[A-Z][a-z]*$");
    }// regex to check name


    abstract boolean isValidAge(int age);
    // Two different validation rules for age (For Student and Teacher case)
    // two different validation rules: override method for each class

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(isValidName(name))
            this.name = name;
        else
            throw new RuntimeException("Not a valid name");
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(isValidAge(age))
            this.age = age;
        else
            throw new RuntimeException("Not a valid age");
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
