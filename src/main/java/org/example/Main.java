package org.example;

import org.example.input.StudentInput;
import org.example.input.TeacherInput;
import org.example.task.GenericStorage;
import org.example.task.Person;
import org.example.task.Student;
import org.example.task.Teacher;

import static java.lang.Thread.sleep;
import static org.example.data.Data.generateStudentInputs;
import static org.example.data.Data.generateTeacherInputs;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        GenericStorage<Person> genericStorage = new GenericStorage<>();

        for (TeacherInput input : generateTeacherInputs()){// separated input format and data to different package
            try {
                // Validation for name:
                // - first letter capitalized
                // - following letters must be lowercase (name cannot contain other characters or digits)
                // Validation for teacher age:
                // - (it must be more than 25 and less than 65)
                Teacher t = new Teacher(input.getName(),  input.getAge());
                genericStorage.addItem(t);
                System.out.println(t + " Added ");
            } catch (RuntimeException ex) {
                // handles exception in the case of invalid inputs
                System.err.println(input + ": " + ex.getMessage());
            }
            sleep(300);// add a delay to see the console output on time
        }

        for(StudentInput input : generateStudentInputs()) {// separated input format and data to different package
            try {
                // Validation for name:
                // - first letter capitalized
                // - following letters must be lowercase (name cannot contain other characters or digits)
                // Validation for student age:
                // - (it must be more than 18 and less than 35)
                Student s = new Student(input.getName(), input.getAge());
                genericStorage.addItem(s);
                System.out.println(s + " Added ");
            } catch (RuntimeException ex) {
                // handles exception in the case of invalid inputs
                System.err.println(input + ": " + ex.getMessage());
            }
            sleep(300);// add a delay to see the console output on time
        }

        System.out.println();
        System.out.println("Data in the storage:");
        System.out.println("------------------");
        genericStorage.displayAllItems();
        System.out.println("------------------");

        System.out.println();
        System.out.println("Search Result: Person with id of 55: ");
        System.out.println(genericStorage.searchItem(55) == null ? "Person not found" :
                genericStorage.searchItem(55));
        System.out.println();
        System.out.println("Search Result: Person with id of 2: ");
        System.out.println(genericStorage.searchItem(2) == null ? "Person not found" :
                genericStorage.searchItem(2));

        System.out.println();
        System.out.println("Remove person with id of 2:");
        genericStorage.removeItem(2);

        System.out.println();
        System.out.println("Remove person with id of 1:");
        genericStorage.removeItem(1);

        System.out.println();
        System.out.println("Remove person with id of 7:");
        genericStorage.removeItem(7);

        System.out.println();
        System.out.println("Data in the storage:");
        System.out.println("------------------");
        genericStorage.displayAllItems();
        System.out.println("------------------");
    }
}