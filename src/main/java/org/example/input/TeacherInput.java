package org.example.input;

public class TeacherInput {// Structure to store teacher input data
    private String name;
    private int age;

    public TeacherInput(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "TeacherInput{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
