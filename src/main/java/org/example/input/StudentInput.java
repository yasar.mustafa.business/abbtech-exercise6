package org.example.input;

public class StudentInput {// Structure to store student input data
    private String name;
    private int age;


    public StudentInput(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "StudentInput{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}