package org.example.data;

import org.example.input.StudentInput;
import org.example.input.TeacherInput;

public class Data {// sample data for student and teacher
    public static TeacherInput[] generateTeacherInputs(){
        TeacherInput tp1 = new TeacherInput("JOhn", 85);
        TeacherInput tp2 = new TeacherInput("David", 65);
        TeacherInput tp3 = new TeacherInput("Smith", 44);
        TeacherInput tp4 = new TeacherInput("Jane", 15);
        TeacherInput tp5 = new TeacherInput("Douglas", 54);
        TeacherInput tp6 = new TeacherInput("Thomas", 32);
        TeacherInput tp7 = new TeacherInput("Ferdinand", 52);
        TeacherInput tp8 = new TeacherInput("Anelka", 63);
        TeacherInput tp9 = new TeacherInput("Heaton", 12);
        return new TeacherInput[]{tp1, tp2, tp3, tp4, tp5, tp6, tp7, tp8, tp9};
    }

    public static StudentInput[] generateStudentInputs(){
        StudentInput sp1 = new StudentInput("Lu8iz", 85);
        StudentInput sp2 = new StudentInput("Onana", 20);
        StudentInput sp3 = new StudentInput("Altay", 22);
        StudentInput sp4 = new StudentInput("Jim", 45);
        StudentInput sp5 = new StudentInput("Karim", 24);

        return new StudentInput[]{sp1, sp2, sp3, sp4, sp5};
    }
}
